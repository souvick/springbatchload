package com.batch.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class User {

	@Id
    private Integer id;
    private String name;
    private String skill;
    private Date time;
    
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", skill=" + skill + ", time=" + time + "]";
	}
	
	
}
