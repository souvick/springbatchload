package com.batch.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.batch")
@EnableJpaRepositories("com.batch.repository")
@EntityScan("com.batch.model")

public class SpringbatchloadApplication {
	
	

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchloadApplication.class, args);
	}
	
	
	

}
