package com.batch.core;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.batch.model.User;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<User, User> {

    private static final Map<String, String> SKILL_NAMES =
            new HashMap<>();

    public Processor() {
    	SKILL_NAMES.put("001", "JAVA");
    	SKILL_NAMES.put("002", "Python");
    	SKILL_NAMES.put("003", "C");
    }

    @Override
    public User process(User user) throws Exception {
        String skillCode = user.getSkill();
        String skillName = SKILL_NAMES.get(skillCode);
        user.setSkill(skillName);
        user.setTime(new Date());
        System.out.println(String.format("Converted from [%s] to [%s]", skillCode, skillName));
        return user;
    }
}
